package olc2_proyecto1;

import Analizadores.CHTML.*;
import Analizadores.CJS.*;
import Analizadores.CCSS.*;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * @201213388
 */
public class Tab extends javax.swing.JPanel {

    private final String absPath, testCHTML, testCCSS, testCJS;
    private String archivo;
    public String tabName;

    CHTML_Arbol achtml = new CHTML_Arbol();
    JScrollPane scrollPane;

    public Tab() {
        initComponents();
        tabName = "Test";
        absPath = "C:\\Users\\RJ\\version-control\\OLC2\\S1_2018\\Proyecto1\\Entradas\\Archivos_prueba\\";
        testCHTML = absPath + "prueba.chtml";
        //testCHTML = absPath + "simples.chtml";
        testCCSS = absPath + "simples.ccss";
        testCJS = absPath + "simples.cjs";
        //testCHTML = absPath + "ejemploEnunciado.chtml";
        //testCCSS = absPath + "ejemploEnunciado.ccss";
        //testCJS = absPath + "ejemploEnunciado.cjs";
    }

    private void verificarLista(LinkedList<TError> lista, String ambito) {
        if (!lista.isEmpty()) {
            System.out.println("Se encontraron " + lista.size() + " error(es)");
            String salida;
            for (TError err : lista) {
                salida = "\t" + ambito + ":= Lexema[" + err.lexema + "], Linea["
                        + err.linea + "], Columna[" + err.columna + "], Desc["
                        + err.descripcion + "]";
                System.out.println(salida);
            }
        }
    }

    private String getArchivoContent(String url) {
        String cadena;
        String cadenaX = "";
        try {
            File arch = new File(url);
            if (!arch.exists()) {
                System.out.println("El archivo no existe! PATH: " + url);
                return "";
            }
            FileReader lector = new FileReader(arch);
            BufferedReader bufer = new BufferedReader(lector);

            cadena = bufer.readLine();
            while (cadena != null) {
                cadenaX += cadena + "\n";
                cadena = bufer.readLine();
            }

            bufer.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Interfaz.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Interfaz.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cadenaX;
    }

    private void CompilarCHTML(String strEntrada) {
        CHTML_Lexico.listaErrores.clear();
        CHTML_Sintactico.listaErrores.clear();
        System.out.println("\n<Inicio compilacion CHTML>");

        CHTML_Lexico lexico = new CHTML_Lexico(new BufferedReader(new StringReader(strEntrada)));
        CHTML_Sintactico sintactico = new CHTML_Sintactico(lexico);

        try {
            sintactico.parse();
        } catch (Exception e) {
        }
        verificarLista(CHTML_Lexico.listaErrores, "CHTML - Error Lexico");
        verificarLista(CHTML_Sintactico.listaErrores, "CHTML - Error Sintactico");
    }

    private void CompilarCCSS(String strEntrada) {
        CCSS_Lexico.listaErrores.clear();
        CCSS_Sintactico.listaErrores.clear();
        System.out.println("\n<Inicio compilacion CCSS>");

        CCSS_Lexico lexico = new CCSS_Lexico(new BufferedReader(new StringReader(strEntrada)));
        CCSS_Sintactico sintactico = new CCSS_Sintactico(lexico);

        try {
            sintactico.parse();
        } catch (Exception e) {
        }
        verificarLista(CCSS_Lexico.listaErrores, "CCSS - Error Lexico");
        verificarLista(CCSS_Sintactico.listaErrores, "CCSS - Error Sintactico");
    }

    private void CompilarCJS(String strEntrada) {
        CJS_Lexico.listaErrores.clear();
        CJS_Sintactico.listaErrores.clear();
        System.out.println("\n<Inicio compilacion CJS>");

        CJS_Lexico lexico = new CJS_Lexico(new BufferedReader(new StringReader(strEntrada)));
        CJS_Sintactico sintactico = new CJS_Sintactico(lexico);

        try {
            sintactico.parse();
        } catch (Exception e) {
        }
        verificarLista(CJS_Lexico.listaErrores, "CJS - Error Lexico");
        verificarLista(CJS_Sintactico.listaErrores, "CJS - Error Sintactico");
    }

    public void crearTab() {
        if (!tabName.equals(achtml.web_panel.getName())) {
            tabName = achtml.web_panel.getName();
            scrollPane = new JScrollPane(achtml.web_panel);
            scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
            
            //this.jtp_favoritos.add(scrollPane, BorderLayout.CENTER);
            this.jtp_favoritos.add(scrollPane, achtml.web_panel.getName());
            this.jtp_favoritos.setSelectedComponent(scrollPane);
        } else {
            this.jtp_favoritos.setSelectedComponent(scrollPane);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton7 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtURL = new javax.swing.JTextField();
        btn_reload = new javax.swing.JButton();
        btn_foward = new javax.swing.JButton();
        btn_back = new javax.swing.JButton();
        jtp_favoritos = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtTest = new javax.swing.JTextArea();
        btnTest = new javax.swing.JButton();
        btnCompilar = new javax.swing.JButton();
        btnCHTML = new javax.swing.JButton();
        btnCCSS = new javax.swing.JButton();
        btnCJS = new javax.swing.JButton();
        statusLabel = new javax.swing.JLabel();

        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Favorite.png"))); // NOI18N
        jButton7.setContentAreaFilled(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("USAC-WEB");

        btn_reload.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Reload.png"))); // NOI18N
        btn_reload.setContentAreaFilled(false);
        btn_reload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_reloadActionPerformed(evt);
            }
        });

        btn_foward.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Foward.png"))); // NOI18N
        btn_foward.setContentAreaFilled(false);
        btn_foward.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_fowardActionPerformed(evt);
            }
        });

        btn_back.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Back.png"))); // NOI18N
        btn_back.setContentAreaFilled(false);
        btn_back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_backActionPerformed(evt);
            }
        });

        txtTest.setColumns(20);
        txtTest.setRows(5);
        jScrollPane2.setViewportView(txtTest);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 658, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 296, Short.MAX_VALUE)
        );

        jtp_favoritos.addTab("Editor", jPanel1);

        btnTest.setText("Test");
        btnTest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestActionPerformed(evt);
            }
        });

        btnCompilar.setText("Compilar");
        btnCompilar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCompilarActionPerformed(evt);
            }
        });

        btnCHTML.setText("Compilar CHTML");
        btnCHTML.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCHTMLActionPerformed(evt);
            }
        });

        btnCCSS.setText("Compilar CCSS");
        btnCCSS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCCSSActionPerformed(evt);
            }
        });

        btnCJS.setText("Compilar CJS");
        btnCJS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCJSActionPerformed(evt);
            }
        });

        statusLabel.setText("status: ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 663, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btn_back, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_foward, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton7, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_reload, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtURL))
                    .addComponent(jtp_favoritos)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(statusLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnTest)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCompilar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCHTML)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCCSS)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCJS)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn_foward, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_reload, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtURL)
                    .addComponent(btn_back))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jtp_favoritos)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCJS)
                    .addComponent(btnCCSS)
                    .addComponent(btnCHTML)
                    .addComponent(btnCompilar)
                    .addComponent(btnTest)
                    .addComponent(statusLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btn_reloadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_reloadActionPerformed
        //C:\Users\RJ\version-control\OLC2\S1_2018\Proyecto1\Entradas\Archivos_prueba\ejemploEnunciado.chtml
        archivo = txtURL.getText();
        String contenido = getArchivoContent(archivo);
        txtTest.setText(contenido);
        CompilarCHTML(getArchivoContent(archivo));
    }//GEN-LAST:event_btn_reloadActionPerformed

    private void btn_fowardActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_fowardActionPerformed
        txtURL.setText("C:\\Users\\RJ\\version-control\\OLC2\\S1_2018\\Proyecto1\\Entradas\\Archivos_prueba\\ejemploEnunciado.chtml");
    }//GEN-LAST:event_btn_fowardActionPerformed

    private void btn_backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_backActionPerformed

    }//GEN-LAST:event_btn_backActionPerformed

    private void btnTestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestActionPerformed
        System.out.println("\nTEST:");
        achtml.AccionesCHTML();
        //CHTML_Arbol.AccionesCHTML();
        //CCSS_Arbol.AccionesCCSS();
        //CJS_Arbol.AccionesCJS();
    }//GEN-LAST:event_btnTestActionPerformed

    private void btnCompilarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCompilarActionPerformed
        crearTab();
    }//GEN-LAST:event_btnCompilarActionPerformed

    private void btnCHTMLActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCHTMLActionPerformed
        String datos = getArchivoContent(testCHTML);
        this.txtTest.setText(datos);
        CompilarCHTML(datos);
    }//GEN-LAST:event_btnCHTMLActionPerformed

    private void btnCCSSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCCSSActionPerformed
        String datos = getArchivoContent(testCCSS);
        this.txtTest.setText(datos);
        CompilarCCSS(datos);
    }//GEN-LAST:event_btnCCSSActionPerformed

    private void btnCJSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCJSActionPerformed
        String datos = getArchivoContent(testCJS);
        this.txtTest.setText(datos);
        CompilarCJS(datos);
    }//GEN-LAST:event_btnCJSActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCCSS;
    private javax.swing.JButton btnCHTML;
    private javax.swing.JButton btnCJS;
    private javax.swing.JButton btnCompilar;
    private javax.swing.JButton btnTest;
    private javax.swing.JButton btn_back;
    private javax.swing.JButton btn_foward;
    private javax.swing.JButton btn_reload;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jtp_favoritos;
    private javax.swing.JLabel statusLabel;
    private javax.swing.JTextArea txtTest;
    private javax.swing.JTextField txtURL;
    // End of variables declaration//GEN-END:variables
}
