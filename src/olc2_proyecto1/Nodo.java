package olc2_proyecto1;

import java.util.ArrayList;

public class Nodo {
    public String TAG; //VALUE;
    public boolean hasAtributes = false;
    public ArrayList<Nodo> listaHijos;
    public ArrayList<String> listaValores;

    //Crear ETIQUETA con solo tag
    public Nodo(String tag) {
        this.TAG = tag;
        this.listaHijos = new ArrayList<>();
        this.listaValores = new ArrayList<>();
    }
    
    //Crear ETIQUETA con tag y agregar una cadena a lista value
    public Nodo(String tag, String value) {
        this.TAG = tag;
        this.listaHijos = new ArrayList<>();
        this.listaValores = new ArrayList<>();
        this.listaValores.add(value);
    }

    //Crear una etiqueta, agregar valor y 1 hijo
    public Nodo(String tag, String value, Nodo hijo) {
        this.TAG = tag;
        this.listaValores = new ArrayList<>();
        this.listaValores.add(value);
        this.listaHijos = new ArrayList<>();
        if(hijo != null){
            this.listaHijos.add(hijo);
        }
    }

    //Crear etiqueta, con atributos = true y adjuntar nodo atrib
    public Nodo(String tag, boolean hasAtributo, Nodo hijo){
        this.TAG = tag;
        this.hasAtributes = hasAtributo;
        this.listaValores = new ArrayList<>();
        this.listaHijos = new ArrayList<>();
        if(hijo != null){
            this.listaHijos.add(hijo);
        }
    }
    
    //Crear una etiqueta y adjuntar una lista
    public Nodo(String tag, ArrayList<Nodo> lista) {
        this.TAG = tag;
        this.listaValores = new ArrayList<>();
        if(lista != null){
        this.listaHijos = lista;
        } else {
            this.listaHijos = new ArrayList<>();
        }
    }
    
    //Crear una etiqueta con 1 hijo
    public Nodo(String tag, Nodo hijo) {
        this.TAG = tag;
        this.listaValores = new ArrayList<>();
        this.listaHijos = new ArrayList<>();
        if(hijo != null){
            this.listaHijos.add(hijo);
        }
    }

    //Crear una etiqueta y adjungar 2 hijos
    public Nodo(String tag, Nodo hijo1, Nodo hijo2) {
        this.TAG = tag;
        this.listaHijos = new ArrayList<>();
        this.listaValores = new ArrayList<>();
        if(hijo1 != null){
            this.listaHijos.add(hijo1);
        }
        if(hijo2 != null){
            this.listaHijos.add(hijo2);
        }
    }

    //Crear una etiqueta y adjungar 2 valores
    public Nodo(String tag, String value1, String value2) {
        this.TAG = tag;
        this.listaHijos = new ArrayList<>();
        this.listaValores = new ArrayList<>();
        this.listaValores.add(value1);
        this.listaValores.add(value2);
    }
    
//---------------------------------------------------------------------
//-------------------------    Setters & Getters
//---------------------------------------------------------------------
    
    //Retorna el primer valor de la lista listaValores
    public String getVALUE(){
        if(listaValores == null) return null;
        if(listaValores.isEmpty()) return null;
        
        return listaValores.get(0);
    }
    
    //Limpia y agrega a la posicion inicial el valor indicado
    public void setVALUE(String value){
        if(listaValores == null) listaValores = new ArrayList<>();
        
        listaValores.clear();
        listaValores.add(value);
    }

    //Agregar String al String que se encuentra en la posicion inicial
    public void addVALUE(String value){
        if(listaValores == null) return;
        if(listaValores.isEmpty()) listaValores.add(value);
        
        listaValores.add(0, listaValores.get(0) + " " + value);
    }
    
   //Retorna el segundo valor de la lista listaValores
    public String getSecondValue(){
        if(listaValores == null) return null;
        if(listaValores.size() < 2) return null;
        
        return listaValores.get(1);
    }
    
    public boolean hasVALUE(){
        if(listaValores == null) return false;
        
        return !listaValores.isEmpty();
    }
    
}
