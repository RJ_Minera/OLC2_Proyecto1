/*--------------- 1ra Area: Codigo de Usuario -----------------------*/
//-------> importaciones, paquetes
package Analizadores.CHTML;

import olc2_proyecto1.Nodo;
import olc2_proyecto1.TError;
import java_cup.runtime.Symbol;
import java.util.LinkedList;

//------> Codigo para el parser,variables, metodos
parser code
{:
    public static Nodo raiz;
    public static LinkedList<TError> listaErrores = new LinkedList<TError>(); 

    //Metodo al que se llama automaticamente ante algun error sintactico
    public void syntax_error(Symbol s)
    {
        System.out.print("\n[Recuperado]"); 
        
        String lexema = s.value.toString();
        int fila = s.right + 1;
        int columna = s.left + 1;
        
        TError datos = new TError(1,lexema,fila,columna);
        listaErrores.add(datos); 
    }

    //Metodo al que se llama en el momento en que ya no es posible una recuperacion de errores
    public void unrecovered_syntax_error(Symbol s) throws java.lang.Exception
    {
        System.out.println("\t[Panic Mode!]"); 
    }
:}

//------> Codigo para las acciones gramaticales
action code
{:
:}

//---------------------------------------------------------------------
//-------------------------    Declarar terminales
//---------------------------------------------------------------------

terminal    tk_numero,tk_cadena,tk_id,tk_punct;
terminal    EtOp,EtCl,tk_igual,tk_ptcoma;//parent_op,parent_cl;
terminal    pr_fondo,pr_ruta,pr_click,pr_valor,pr_id,pr_grupo,pr_alto,pr_ancho,pr_alineado;
terminal    op_chtml,op_encabezado,op_cjs,op_ccss,op_cuerpo,op_titulo,op_panel,op_texto,op_imagen,
            op_boton, op_enlace,op_tabla,op_filt,op_cb,op_ct,op_texto_a,op_caja_texto,op_caja,
            op_opcion,op_spinner;
terminal    cl_chtml,cl_encabezado,cl_cjs,cl_ccss,cl_cuerpo,cl_titulo,cl_panel,cl_texto,cl_imagen,
            cl_boton,cl_enlace,cl_tabla,cl_filt,cl_cb,cl_ct,cl_texto_a,cl_caja_texto,cl_caja,
            cl_opcion,cl_spinner,cl_salto;

//---------------------------------------------------------------------
//-------------------------    Declarar no terminales
//---------------------------------------------------------------------

non terminal Nodo INICIO, CHTML, ENCABEZADO, ENCABEZADO_LT, CJS, CCSS, TITULO;
non terminal Nodo AT_ELEMENTOS_CCSS, ELEMENTO_CCSS, AT_FONDO, AT_RUTA, 
                  AT_CLICK, AT_VALOR;
non terminal Nodo CUERPO, CUERPO_CONT, CUERPO_LT, PANEL, SPINNER, SALTO;
non terminal Nodo TEXT_IN_TAGS, TEXTO, IMAGEN, IMAGEN_AT, BOTON, 
                  BOTON_AT, ENLACE, ENLACE_AT, TEXTO_A, CAJA_TEXTO;
non terminal Nodo TABLA, TABLA_LT, FILT, FILT_LT, TAB_CB, TAB_CT;
non terminal Nodo CAJA, AT_CAJA, CAJA_LT, OPCION, AT_OPCION;
non terminal String TEXT_IN_VAL, PR_ELEMENTO_CCSS;

//---------------------------------------------------------------------
//-------------------------    Inicio Gramatica
//---------------------------------------------------------------------

start with INICIO ;

INICIO::= CHTML:raiz
            {: 
                parser.raiz = raiz;
            :}
        | error INICIO
        ;

CHTML::= EtOp op_chtml EtCl ENCABEZADO:h1 CUERPO:h2 cl_chtml {: RESULT = new Nodo("CHTML",h1,h2); :} ;

ENCABEZADO::= EtOp op_encabezado EtCl ENCABEZADO_LT:lista cl_encabezado 
            {: 
                RESULT = new Nodo("ENCABEZADO",lista.listaHijos);
            :}
        | EtOp op_encabezado EtCl cl_encabezado {: RESULT = new Nodo("ENCABEZADO"); :}
        ;
ENCABEZADO_LT::= ENCABEZADO_LT:lista CJS:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;

            :}
        | ENCABEZADO_LT:lista CCSS:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;

            :}
        | ENCABEZADO_LT:lista TITULO:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;

            :}
        | CJS:h1 {: RESULT = new Nodo("ENCABEZADO_LT",h1); :}
        | CCSS:h1 {: RESULT = new Nodo("ENCABEZADO_LT",h1); :}
        | TITULO:h1 {: RESULT = new Nodo("ENCABEZADO_LT",h1); :}
        | error EtCl
        ;

TITULO::= EtOp op_titulo EtCl TEXT_IN_TAGS:txt cl_titulo {: RESULT = new Nodo("TITULO",txt.getVALUE()); :}
        | EtOp op_titulo EtCl cl_titulo {: RESULT = new Nodo("TITULO"); :}
        ;
CJS::= EtOp op_cjs AT_RUTA:atrib EtCl cl_cjs
            {:
                RESULT = new Nodo("CJS",atrib.getVALUE());
            :}
        ;
CCSS::= EtOp op_ccss AT_RUTA:atrib EtCl cl_ccss 
            {:
                RESULT = new Nodo("CCSS",atrib.getVALUE());
            :}
        ;

//---------------------------------------------------------------------
//-------------------------    Elementos CCSS - Atributos
//---------------------------------------------------------------------

AT_ELEMENTOS_CCSS::= AT_ELEMENTOS_CCSS:lista ELEMENTO_CCSS:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;
            :}
        | ELEMENTO_CCSS:h1 {: RESULT = new Nodo("AT_ELEMENTOS_CCSS",h1); :}
        | error tk_ptcoma
        ;

ELEMENTO_CCSS::= PR_ELEMENTO_CCSS:pr_str tk_igual tk_cadena:cad tk_ptcoma
            {: 
                RESULT = new Nodo("ELEMENTO_CCSS",cad.toString(),pr_str);
            :}
        ;
PR_ELEMENTO_CCSS::= pr_id {: RESULT = "pr_id"; :}
        | pr_grupo {: RESULT = "pr_grupo"; :}
        | pr_alto {: RESULT = "pr_alto"; :}
        | pr_ancho {: RESULT = "pr_ancho"; :}
        | pr_alineado {: RESULT = "pr_alineado"; :}
        ;

AT_FONDO::=pr_fondo tk_igual tk_cadena:val tk_ptcoma {: RESULT = new Nodo("AT_FONDO",val.toString()); :} ;
AT_CLICK::= pr_click tk_igual tk_cadena:val tk_ptcoma {: RESULT = new Nodo("AT_CLICK",val.toString()); :} ;
AT_RUTA::= pr_ruta tk_igual tk_cadena:val tk_ptcoma {: RESULT = new Nodo("AT_RUTA",val.toString()); :} ;
AT_VALOR ::= pr_valor tk_igual tk_cadena:val tk_ptcoma {: RESULT = new Nodo("AT_VALOR",val.toString()); :} ;

//---------------------------------------------------------------------
//-------------------------    Cuerpo
//---------------------------------------------------------------------

CUERPO::= EtOp op_cuerpo AT_FONDO:atrib EtCl CUERPO_CONT:cont cl_cuerpo
            {: 
                RESULT = new Nodo("CUERPO",atrib,cont);
            :}
        | EtOp op_cuerpo EtCl CUERPO_CONT:cont cl_cuerpo
            {: 
                RESULT = new Nodo("CUERPO",cont);
            :}
        | EtOp op_cuerpo AT_FONDO:atrib EtCl cl_cuerpo
            {: 
                RESULT = new Nodo("CUERPO",atrib);
            :}
        | EtOp op_cuerpo EtCl cl_cuerpo
            {: 
                RESULT = new Nodo("CUERPO");
            :}
        | error EtCl
        ;
CUERPO_CONT::= CUERPO_CONT:lista CUERPO_LT:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;
            :}
        | CUERPO_LT:h1
            {:
                RESULT = new Nodo("CUERPO_CONT",h1);
            :}
        | error EtCl
        ;
CUERPO_LT::= PANEL:nd {: RESULT = nd; :}
        | TEXTO:nd {: RESULT = nd; :} 
        | IMAGEN:nd {: RESULT = nd; :} 
        | BOTON:nd {: RESULT = nd; :} 
        | ENLACE:nd {: RESULT = nd; :} 
        | TABLA:nd {: RESULT = nd; :} 
        | TEXTO_A:nd {: RESULT = nd; :} 
        | CAJA_TEXTO:nd {: RESULT = nd; :} 
        | CAJA:nd {: RESULT = nd; :} 
        | SPINNER:nd {: RESULT = nd; :} 
        | SALTO:nd {: RESULT = nd; :} 
        ;

PANEL::= EtOp op_panel AT_ELEMENTOS_CCSS:atrib EtCl CUERPO_CONT:cont cl_panel
            {:
                Nodo nd = new Nodo("PANEL",true,atrib);
                nd.listaHijos.add(cont);
                RESULT = nd;
            :}
        | EtOp op_panel EtCl CUERPO_CONT:cont cl_panel
            {:
                RESULT = new Nodo("PANEL",cont);
            :}
        | EtOp op_panel AT_ELEMENTOS_CCSS:atrib EtCl cl_panel
            {:
                RESULT = new Nodo("PANEL",true,atrib);
            :}
        | EtOp op_panel EtCl cl_panel
            {:
                RESULT = new Nodo("PANEL");
            :}
        ; 

SPINNER::= EtOp op_spinner AT_ELEMENTOS_CCSS:atrib EtCl tk_numero:val cl_spinner
            {: 
                Nodo nd = new Nodo("SPINNER",true,atrib);
                nd.listaValores.add(val.toString());
                RESULT = nd;
            :}
        | EtOp op_spinner EtCl tk_numero:val cl_spinner
            {: 
                RESULT = new Nodo("SPINNER",val.toString());
            :}
        | EtOp op_spinner AT_ELEMENTOS_CCSS:atrib EtCl cl_spinner
            {: 
                RESULT = new Nodo("SPINNER",true,atrib);
            :}
        | EtOp op_spinner EtCl cl_spinner
            {: 
                RESULT = new Nodo("SPINNER");
            :}
        ;

SALTO::= cl_salto {: RESULT = new Nodo("SALTO"); :};

//---------------------------------------------------------------------
//-------------------------    Tabla
//---------------------------------------------------------------------

TABLA::= EtOp op_tabla AT_ELEMENTOS_CCSS:atrib EtCl TABLA_LT:cont cl_tabla
            {:
                Nodo nd = new Nodo("TABLA",true,atrib);
                nd.listaHijos.add(cont);
                RESULT = nd;
            :}
        | EtOp op_tabla EtCl TABLA_LT:cont cl_tabla
            {:
                RESULT = new Nodo("TABLA",cont); 
            :}
        | EtOp op_tabla EtCl cl_tabla {: RESULT = new Nodo("TABLA"); :}
        ;
TABLA_LT::= TABLA_LT:lista FILT:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;
            :}
        | FILT:h1 {: RESULT = new Nodo("TABLA_LT",h1); :}
        | error EtCl
        ;
FILT::= EtOp op_filt AT_ELEMENTOS_CCSS:atrib EtCl FILT_LT:cont cl_filt
            {:
                Nodo nd = new Nodo("FILT",true,atrib);
                nd.listaHijos.add(cont);
                RESULT = nd;
            :}
        | EtOp op_filt EtCl FILT_LT:cont cl_filt
            {:
                RESULT = new Nodo("FILT",cont); 
            :}
        | EtOp op_filt EtCl cl_filt {: RESULT = new Nodo("FILT"); :}
        ;
FILT_LT::= FILT_LT:lista TAB_CB:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista; 
            :}
        | FILT_LT:lista TAB_CT:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista; 
            :}
        | TAB_CB:h1 {: RESULT = new Nodo("FILT_LT",h1); :}
        | TAB_CT:h1 {: RESULT = new Nodo("FILT_LT",h1); :}
        | error EtCl
        ;
TAB_CB::= EtOp op_cb AT_ELEMENTOS_CCSS:atrib EtCl TEXT_IN_TAGS:txt cl_cb
            {:
                Nodo nd = new Nodo("TAB_CB",true,atrib);
                nd.listaValores.add(txt.getVALUE());
                RESULT = nd;
            :}
        | EtOp op_cb EtCl TEXT_IN_TAGS:txt cl_cb
            {:
                RESULT = new Nodo("TAB_CB",txt.getVALUE());
            :}
        | EtOp op_cb EtCl cl_cb
            {:
                RESULT = new Nodo("TAB_CB");
            :}
        ;
TAB_CT::= EtOp op_ct AT_ELEMENTOS_CCSS:atrib EtCl TEXT_IN_TAGS:txt cl_ct
            {:
                Nodo nd = new Nodo("TAB_CT",true,atrib);
                nd.listaValores.add(txt.getVALUE());
                RESULT = nd;
            :}
        | EtOp op_ct EtCl TEXT_IN_TAGS:txt cl_ct
            {:
                RESULT = new Nodo("TAB_CT",txt.getVALUE());
            :}
        | EtOp op_ct EtCl cl_ct
            {:
                RESULT = new Nodo("TAB_CT");
            :}
        ;

//---------------------------------------------------------------------
//-------------------------    Caja
//---------------------------------------------------------------------

CAJA::= EtOp op_caja AT_CAJA:atrib EtCl CAJA_LT:cont cl_caja 
            {: 
                Nodo nd = new Nodo("CAJA",true,atrib);
                nd.listaHijos.add(cont);
                RESULT = nd;
            :}
        | EtOp op_caja EtCl CAJA_LT:cont cl_caja 
            {: 
                RESULT = new Nodo("CAJA",cont); 
            :}
        ;
AT_CAJA::= AT_CAJA:lista ELEMENTO_CCSS:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;
            :}
        | AT_CAJA:lista AT_CLICK:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;
            :}
        | ELEMENTO_CCSS:h1 {: RESULT = new Nodo("AT_CAJA",h1); :}
        | AT_CLICK:h1 {: RESULT = new Nodo("AT_CAJA",h1); :}
        | error tk_ptcoma
        ;
CAJA_LT::= CAJA_LT:lista OPCION:h2 
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;
            :}
        | OPCION:h1 {: RESULT = new Nodo("CAJA_LT",h1); :}
        | error EtCl
        ;

OPCION::= EtOp op_opcion AT_OPCION:atrib EtCl TEXT_IN_TAGS:txt cl_opcion
            {:
                Nodo nd = new Nodo("OPCION",true,atrib);
                nd.listaValores.add(txt.getVALUE());
                RESULT = nd;
            :}
        | EtOp op_opcion EtCl TEXT_IN_TAGS:txt cl_opcion
            {:
                RESULT = new Nodo("OPCION",txt.getVALUE());
            :}
        ;
AT_OPCION::= AT_OPCION:lista ELEMENTO_CCSS:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;
            :}
        | AT_OPCION:lista AT_VALOR:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;
            :}
        | ELEMENTO_CCSS:h1 {: RESULT = new Nodo("AT_OPCION",h1); :}
        | AT_VALOR:h1 {: RESULT = new Nodo("AT_OPCION",h1); :}
        | error tk_ptcoma
        ;

//---------------------------------------------------------------------
//-------------------------    Tags con TEXT_IN_TAGS
//---------------------------------------------------------------------

TEXTO::= EtOp op_texto AT_ELEMENTOS_CCSS:atrib EtCl TEXT_IN_TAGS:txt cl_texto
            {:
                Nodo nd = new Nodo("TEXTO",true,atrib);
                nd.listaValores.add(txt.getVALUE());
                RESULT = nd;
            :}
        | EtOp op_texto AT_ELEMENTOS_CCSS:atrib EtCl cl_texto
            {:
                RESULT = new Nodo("TEXTO",true,atrib);
            :}
        | EtOp op_texto EtCl TEXT_IN_TAGS:txt cl_texto
            {:
                RESULT = new Nodo("TEXTO",txt.getVALUE());
            :}
        | EtOp op_texto EtCl cl_texto
            {:
                RESULT = new Nodo("TEXTO");
            :}
        ;

IMAGEN::= EtOp op_imagen IMAGEN_AT:atrib EtCl TEXT_IN_TAGS:txt cl_imagen
            {:
                Nodo nd = new Nodo("IMAGEN",true,atrib);
                nd.listaValores.add(txt.getVALUE());
                RESULT = nd;

            :}
        | EtOp op_imagen IMAGEN_AT:atrib EtCl cl_imagen
            {:
                RESULT = new Nodo("IMAGEN",true,atrib);
            :}
        | EtOp op_imagen EtCl TEXT_IN_TAGS:txt cl_imagen
            {:
                RESULT = new Nodo("IMAGEN",txt.getVALUE());
            :}
        | EtOp op_imagen EtCl cl_imagen
            {:
                RESULT = new Nodo("IMAGEN");
            :}
        ;
IMAGEN_AT::= IMAGEN_AT:lista AT_RUTA:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;
            :}
        | IMAGEN_AT:lista ELEMENTO_CCSS:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;
            :}
        | AT_RUTA:h1 {: RESULT = new Nodo("IMAGEN_AT",h1); :}
        | ELEMENTO_CCSS:h1 {: RESULT = new Nodo("IMAGEN_AT",h1); :}
        | error tk_ptcoma
        ; 

BOTON::= EtOp op_boton BOTON_AT:atrib EtCl TEXT_IN_TAGS:txt cl_boton
            {:
                Nodo nd = new Nodo("BOTON",true,atrib);
                nd.listaValores.add(txt.getVALUE());
                RESULT = nd;
            :}
        | EtOp op_boton BOTON_AT:atrib EtCl cl_boton
            {:
                RESULT = new Nodo("BOTON",true,atrib);
            :}
        | EtOp op_boton EtCl TEXT_IN_TAGS:txt cl_boton
            {:
                RESULT = new Nodo("BOTON",txt.getVALUE());
            :}
        | EtOp op_boton EtCl cl_boton
            {:
                RESULT = new Nodo("BOTON");
            :}
        ;
BOTON_AT::= BOTON_AT:lista ELEMENTO_CCSS:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;
            :}
        | BOTON_AT:lista AT_RUTA:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;
            :}
        | BOTON_AT:lista AT_CLICK:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;
            :}
        | ELEMENTO_CCSS:h1 {: RESULT = new Nodo("BOTON_AT",h1); :}
        | AT_RUTA:h1 {: RESULT = new Nodo("BOTON_AT",h1); :}
        | AT_CLICK:h1 {: RESULT = new Nodo("BOTON_AT",h1); :}
        | error tk_ptcoma
        ;

ENLACE::= EtOp op_enlace ENLACE_AT:atrib EtCl TEXT_IN_TAGS:txt cl_enlace
            {:
                Nodo nd = new Nodo("ENLACE",true,atrib);
                nd.listaValores.add(txt.getVALUE());
                RESULT = nd;
            :}
        | EtOp op_enlace EtCl TEXT_IN_TAGS:txt cl_enlace
            {:
                RESULT = new Nodo("ENLACE",txt.getVALUE());
            :}
        ;
ENLACE_AT::= ENLACE_AT:lista ELEMENTO_CCSS:h2
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;
            :}
        | ENLACE_AT:lista AT_RUTA:h2 
            {:
                lista.listaHijos.add(h2);
                RESULT = lista;
            :}
        | ELEMENTO_CCSS:h1 {: RESULT = new Nodo("ENLACE_AT",h1); :}
        | AT_RUTA:h1 {: RESULT = new Nodo("ENLACE_AT",h1); :}
        | error tk_ptcoma
        ;

TEXTO_A::= EtOp op_texto_a AT_ELEMENTOS_CCSS:atrib EtCl TEXT_IN_TAGS:txt cl_texto_a
            {:
                Nodo nd = new Nodo("TEXTO_A",true,atrib);
                nd.listaValores.add(txt.getVALUE());
                RESULT = nd;
            :}
        | EtOp op_texto_a AT_ELEMENTOS_CCSS:atrib EtCl cl_texto_a
            {:
                RESULT = new Nodo("TEXTO_A",true,atrib);
            :}
        | EtOp op_texto_a EtCl TEXT_IN_TAGS:txt cl_texto_a
            {:
                RESULT = new Nodo("TEXTO_A",txt.getVALUE());
            :}
        | EtOp op_texto_a EtCl cl_texto_a
            {:
                RESULT = new Nodo("TEXTO_A");
            :}
        ;

CAJA_TEXTO::= EtOp op_caja_texto AT_ELEMENTOS_CCSS:atrib EtCl TEXT_IN_TAGS:txt cl_caja_texto
            {:
                Nodo nd = new Nodo("CAJA_TEXTO",true,atrib);
                nd.listaValores.add(txt.getVALUE());
                RESULT = nd;
            :}
        | EtOp op_caja_texto AT_ELEMENTOS_CCSS:atrib EtCl cl_caja_texto
            {:
                RESULT = new Nodo("CAJA_TEXTO",true,atrib);
            :}
        | EtOp op_caja_texto EtCl TEXT_IN_TAGS:txt cl_caja_texto
            {:
                RESULT = new Nodo("CAJA_TEXTO",txt.getVALUE());
            :}
        | EtOp op_caja_texto EtCl cl_caja_texto
            {:
                RESULT = new Nodo("CAJA_TEXTO");
            :}
        ;

TEXT_IN_TAGS::= TEXT_IN_TAGS:lista TEXT_IN_VAL:txt
            {:
                lista.addVALUE(txt);
                RESULT = lista;
            :}
        | TEXT_IN_VAL:txt {: RESULT = new Nodo("TEXT_IN_TAGS",txt); :}
        | error EtOp
        ;
TEXT_IN_VAL::= tk_id:val {: RESULT = val.toString(); :}
        | tk_numero:val {: RESULT = val.toString(); :}
        | tk_punct:val {: RESULT = val.toString(); :}
        | EtCl:val {: RESULT = val.toString(); :}
        | tk_igual:val {: RESULT = val.toString(); :}
        | tk_ptcoma:val {: RESULT = val.toString(); :}               
        | op_chtml:val {: RESULT = val.toString(); :}                 
        | op_encabezado:val {: RESULT = val.toString(); :}     
        | op_cjs:val {: RESULT = val.toString(); :}            
        | op_ccss:val {: RESULT = val.toString(); :}           
        | op_cuerpo:val {: RESULT = val.toString(); :}
        | op_titulo:val {: RESULT = val.toString(); :}
        | op_panel:val {: RESULT = val.toString(); :}
        | op_texto:val {: RESULT = val.toString(); :}
        | op_imagen:val {: RESULT = val.toString(); :}
        | op_boton:val {: RESULT = val.toString(); :}
        | op_enlace:val {: RESULT = val.toString(); :}
        | op_tabla:val {: RESULT = val.toString(); :}
        | op_filt:val {: RESULT = val.toString(); :}
        | op_cb:val {: RESULT = val.toString(); :}
        | op_ct:val {: RESULT = val.toString(); :}
        | op_texto_a:val {: RESULT = val.toString(); :}
        | op_caja_texto:val {: RESULT = val.toString(); :}
        | op_caja:val {: RESULT = val.toString(); :}
        | op_opcion:val {: RESULT = val.toString(); :}
        | op_spinner:val {: RESULT = val.toString(); :}
        | pr_ruta:val {: RESULT = val.toString(); :}
        | pr_click:val {: RESULT = val.toString(); :}
        | pr_valor:val {: RESULT = val.toString(); :}
        | pr_id:val {: RESULT = val.toString(); :}
        | pr_grupo:val {: RESULT = val.toString(); :}
        | pr_alto:val {: RESULT = val.toString(); :}
        | pr_ancho:val {: RESULT = val.toString(); :}
        | pr_alineado:val {: RESULT = val.toString(); :}
        | cl_salto {: RESULT = "\n"; :}
        ;
