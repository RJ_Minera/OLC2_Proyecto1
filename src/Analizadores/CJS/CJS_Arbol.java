package Analizadores.CJS;

import olc2_proyecto1.Nodo;

public class CJS_Arbol {

    public CJS_Arbol() {
    }
    
    public static void AccionesCJS(){
        Nodo raiz = CJS_Sintactico.raiz;
        if (raiz == null) { System.out.println("Raiz is null"); return; }
        ACCION("RAIZ",raiz);
    }
    
    private static void RecorrerHijos(Nodo padre){
        for (Nodo subEtiqueta : padre.listaHijos) {
            ACCION(padre.TAG,subEtiqueta);
        }
    }
    
    private static void ACCION(String padre, Nodo etiqueta){
        if(etiqueta == null) return;
        
        switch(etiqueta.TAG){
            case "INICIO_LT":
            case "BLQ_INSTRUCTS":
            case "LT_INSTRUCTS":
            case "BLQ_CICLO":
            case "CICLO_INST":
            case "DETENER":
            case "DEC_VAR":
            case "ASIG_VAR":
            case "LT_IDS":
            case "TIPO_DATO":
            case "IDENT":
            case "VECTOR":
            case "TOKEN":
            case "LT_LLAVES":
            case "FUNCION":
            case "LT_PARAM":
            case "METODO":
            case "METODO_ARG":
            case "RETORNAR":
            case "CONDICION":
            case "OPL":
            case "OPR":
            case "OPA":
            case "DOC_EVENT":
            case "OBTENER":
            case "SET_ELEMENTO":
            case "OBSERVADOR":
            case "OBSERVADOR_FUNC":
            case "SI":
            case "SINO":
            case "SELECCIONA":
            case "SELECT_LT":
            case "CASO":
            case "DEFECTO":
            case "PARA":
            case "PARA_ID":
            case "MIENTRAS":
            case "IMPRIMIR":
            case "MENSAJE":
            case "ACCION_GET":
                    System.out.println("TAG " + padre + "-" + etiqueta.TAG + " -> h[" 
                            + etiqueta.listaHijos.size() + "]");
                    RecorrerHijos(etiqueta);
                    break;
            default:
                    System.out.println("\tDEFAULT " + padre + "-" + etiqueta.TAG + " -> h[" 
                            + etiqueta.listaHijos.size() + "]");
                    RecorrerHijos(etiqueta);
                break;
        }
    }
    
}
