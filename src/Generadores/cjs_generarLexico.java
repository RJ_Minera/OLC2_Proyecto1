package Generadores;
import java.io.File;

public class cjs_generarLexico 
{
    public static void main(String[] args) 
    {
        String path="src/Analizadores/CJS/cjs.jflex";
        generarLexico(path);
    } 
    
    public static void generarLexico(String path)
    {
        File file=new File(path);
        jflex.Main.generate(file);
    } 
}
