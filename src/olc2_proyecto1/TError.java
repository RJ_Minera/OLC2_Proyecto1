package olc2_proyecto1;

public class TError {
    String lexema,tipo,descripcion;
    int linea,columna;
    
    public TError(int tipo, String lexema,int linea, int columna){
        this.lexema = lexema;
        this.linea = linea;
        this.columna = columna;
        
        switch (tipo) {
            case 0:
                this.tipo = "Error Lexico";
                this.descripcion = "Simbolo " + lexema + " no existe en el lenguaje";
                break;
            case 1:
                this.tipo = "Error Sintactico";
                this.descripcion = "Caracter " + lexema + " no esperado";
                break;
            default:
                this.tipo = "Error de ejecucion";
                this.descripcion = lexema;
        }
    }
}
