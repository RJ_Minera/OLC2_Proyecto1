/*------------  1ra Area: Codigo de Usuario ---------*/
//------> Paquetes,importaciones
package Analizadores.CCSS;
import olc2_proyecto1.TError;
import java_cup.runtime.*;
import java.util.LinkedList;

/*------------  2da Area: Opciones y Declaraciones ---------*/
%%
%{
    //----> Codigo de usuario en sintaxis java
    public static LinkedList<TError> listaErrores = new LinkedList<TError>();
%}

//-------> Directivas
%public 
%class CCSS_Lexico
%cupsym CCSS_Simbolos
%cup
%char
%column
%full
%ignorecase
%line
%unicode

/* Espacio blanco */
LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]
WhiteSpace     = {LineTerminator} | [ \t\f]

/* Comentarios */
Comentario_Multilinea = "/*" [^*] ~"*/" | "/*" "*"+ "/"
Comentario_simple = "//" {InputCharacter}* {LineTerminator}?
Comment = {Comentario_Multilinea} | {Comentario_simple}+

/* ER */
ID = [:jletter:] ([:jletterdigit:]|"_")*
numero = [0-9]+ ("."[0-9]+)?
cadena = "\"" ~"\""
booleano =  "true" | "false"
pr_mayuscula = "Mayúscula" | "Mayuscula"
pr_minuscula = "Minúscula" | "Minuscula"

%%

/* Palabras reservadas - Elementos */
<YYINITIAL> "Alineado"       { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_alineado, yycolumn, yyline, yytext()); }
<YYINITIAL> "Formato"        { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_formato, yycolumn, yyline, yytext()); }
<YYINITIAL> "Letra"          { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_letra, yycolumn, yyline, yytext()); }
<YYINITIAL> "Tamtex"         { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_tamtex, yycolumn, yyline, yytext()); }
<YYINITIAL> "Texto"          { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_texto, yycolumn, yyline, yytext()); }
<YYINITIAL> "Fondoelemento"  { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_fondoelem, yycolumn, yyline, yytext()); }
<YYINITIAL> "Visible"        { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_visible, yycolumn, yyline, yytext()); }
<YYINITIAL> "Borde"          { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_borde, yycolumn, yyline, yytext()); }
<YYINITIAL> "Opaque"         { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_opaque, yycolumn, yyline, yytext()); }
<YYINITIAL> "ColorText"        { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_colortext, yycolumn, yyline, yytext()); }
<YYINITIAL> "AutorRedimension" { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_autored, yycolumn, yyline, yytext()); }

/* Palabras reservadas - atributos o valores */
<YYINITIAL> "izquierda"      { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_izquierda, yycolumn, yyline, yytext()); }
<YYINITIAL> "derecha"        { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_derecha, yycolumn, yyline, yytext()); }
<YYINITIAL> "centrado"       { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_centrado, yycolumn, yyline, yytext()); }
<YYINITIAL> "justificado"    { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_justificado, yycolumn, yyline, yytext()); }
<YYINITIAL> "Negrilla"       { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_negrilla, yycolumn, yyline, yytext()); }
<YYINITIAL> "cursiva"        { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_cursiva, yycolumn, yyline, yytext()); }
<YYINITIAL> {pr_mayuscula}   { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_mayuscula, yycolumn, yyline, yytext()); }
<YYINITIAL> {pr_minuscula}   { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_minuscula, yycolumn, yyline, yytext()); }
<YYINITIAL> "capital-T"      { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_capitalt, yycolumn, yyline, yytext()); }
<YYINITIAL> "vertical"         { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_horizontal, yycolumn, yyline, yytext()); }
<YYINITIAL> "horizontal"       { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_vertical, yycolumn, yyline, yytext()); }

/* Palabras reservadas - editado de elementos */
<YYINITIAL> "GRUPO"         { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_grupo, yycolumn, yyline, yytext()); }
<YYINITIAL> "ID"         { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.pr_id, yycolumn, yyline, yytext()); }


<YYINITIAL> {
    /* ER */    
    {booleano}  { System.out.println("Reconocio booleano "+yytext()); return new Symbol(CCSS_Simbolos.tk_bool, yycolumn, yyline, yytext()); }
    {ID}        { System.out.println("Reconocio ID "+yytext()); return new Symbol(CCSS_Simbolos.tk_id, yycolumn, yyline, yytext()); }
    {numero}    { System.out.println("Reconocio numero "+yytext()); return new Symbol(CCSS_Simbolos.tk_numero, yycolumn, yyline, yytext()); }
    {cadena}    { System.out.println("Reconocio cadena "+yytext()); return new Symbol(CCSS_Simbolos.tk_cadena , yycolumn, yyline, yytext()); }
    
    /* caracteres especiales */
    "["     { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.corch_op , yycolumn, yyline, yytext()); }
    "]"     { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.corch_cl , yycolumn, yyline, yytext()); }
    ":"     { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.tk_dospts , yycolumn, yyline, yytext()); }
    "="     { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.tk_igual , yycolumn, yyline, yytext()); }
    ";"     { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.tk_ptcoma , yycolumn, yyline, yytext()); }
    ","     { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.tk_coma , yycolumn, yyline, yytext()); }
    "("     { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.parent_op , yycolumn, yyline, yytext()); }
    ")"     { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.parent_cl , yycolumn, yyline, yytext()); }
    
    "+"     { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.tk_mas , yycolumn, yyline, yytext()); }
    "-"     { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.tk_menos , yycolumn, yyline, yytext()); }
    "*"     { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.tk_por , yycolumn, yyline, yytext()); }
    "/"     { System.out.println("Reconocio "+yytext()); return new Symbol(CCSS_Simbolos.tk_div , yycolumn, yyline, yytext()); }

    /* Espacio en blanco */
      {WhiteSpace}      { /* ignore */ }
    /* Comentarios */
      {Comment}         { System.out.println("Reconocio comentario: " + yytext()); /* ignore */ }
    }


/* error fallback*/
[^]                       { TError datos = new TError(0,yytext(),yyline,yycolumn); listaErrores.add(datos); }