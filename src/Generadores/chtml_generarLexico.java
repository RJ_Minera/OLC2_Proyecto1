package Generadores;
import java.io.File;

public class chtml_generarLexico {
    
    public static void main(String[] args) 
    {
        String destino = "-d src/Analizadores/CHTML/ "; 
        String path="src/Analizadores/CHTML/chtml.jflex";
        generarLexico(path);
    } 
    
    public static void generarLexico(String path)
    {
        File file=new File(path);
        jflex.Main.generate(file);
    } 
}
