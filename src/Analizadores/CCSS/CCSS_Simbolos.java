
//----------------------------------------------------
// The following code was generated by CUP v0.11b 20160615 (GIT 4ac7450)
//----------------------------------------------------

package Analizadores.CCSS;

/** CUP generated class containing symbol constants. */
public class CCSS_Simbolos {
  /* terminals */
  public static final int tk_cadena = 5;
  public static final int corch_cl = 9;
  public static final int pr_opaque = 28;
  public static final int pr_capitalt = 39;
  public static final int tk_cad_comillas = 7;
  public static final int corch_op = 8;
  public static final int pr_formato = 21;
  public static final int tk_mas = 14;
  public static final int tk_por = 16;
  public static final int pr_cursiva = 36;
  public static final int pr_negrilla = 35;
  public static final int pr_justificado = 34;
  public static final int pr_tamtex = 23;
  public static final int tk_ptcoma = 10;
  public static final int pr_letra = 22;
  public static final int pr_fondoelem = 25;
  public static final int pr_alineado = 20;
  public static final int pr_grupo = 18;
  public static final int tk_numero = 4;
  public static final int pr_vertical = 41;
  public static final int pr_derecha = 32;
  public static final int tk_div = 17;
  public static final int pr_autored = 30;
  public static final int EOF = 0;
  public static final int pr_minuscula = 38;
  public static final int error = 1;
  public static final int tk_bool = 2;
  public static final int pr_colortext = 29;
  public static final int pr_izquierda = 31;
  public static final int tk_asignar = 6;
  public static final int tk_coma = 11;
  public static final int pr_visible = 26;
  public static final int parent_cl = 13;
  public static final int parent_op = 12;
  public static final int pr_id = 19;
  public static final int tk_menos = 15;
  public static final int pr_centrado = 33;
  public static final int tk_id = 3;
  public static final int pr_texto = 24;
  public static final int pr_mayuscula = 37;
  public static final int pr_borde = 27;
  public static final int pr_horizontal = 40;
  public static final String[] terminalNames = new String[] {
  "EOF",
  "error",
  "tk_bool",
  "tk_id",
  "tk_numero",
  "tk_cadena",
  "tk_asignar",
  "tk_cad_comillas",
  "corch_op",
  "corch_cl",
  "tk_ptcoma",
  "tk_coma",
  "parent_op",
  "parent_cl",
  "tk_mas",
  "tk_menos",
  "tk_por",
  "tk_div",
  "pr_grupo",
  "pr_id",
  "pr_alineado",
  "pr_formato",
  "pr_letra",
  "pr_tamtex",
  "pr_texto",
  "pr_fondoelem",
  "pr_visible",
  "pr_borde",
  "pr_opaque",
  "pr_colortext",
  "pr_autored",
  "pr_izquierda",
  "pr_derecha",
  "pr_centrado",
  "pr_justificado",
  "pr_negrilla",
  "pr_cursiva",
  "pr_mayuscula",
  "pr_minuscula",
  "pr_capitalt",
  "pr_horizontal",
  "pr_vertical"
  };
}

