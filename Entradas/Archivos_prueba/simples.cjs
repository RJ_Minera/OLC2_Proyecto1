'Comentario simple

DimV NOMBRE: "Henry";
Nombre: "Julia";

'/ Se agregó un comentario
de
tres líneas/'

DimV operacion: 5+5+2 ;

Dimv miVector : { 14 ,240 , 350 };
Dimv nombres : {"Hola", "Mundo" , "Otra" , "Vez" };
DimV nuevoVector : { "Hola" , "Mundo" , 1 , 2 , 3 };
DimV vectorStatico{3};
vectorEstatico : { "Hola" , "Otra vez" , 250};
DimV miNumero: 4;
DimV vector_miNumero{miNumero};
vectorEstatico : { 51 , 200 , 40 , 56};

DimV valorVector;
Dimv miVector : { 14 ,240 , 350 };
'Modificación del vector en la primera posición
miVector{0} : 100;
'Obtener valor del vector
valorVector : miVector{0};
miVector{2} : valorVector + miVector{1};

DimV tamanioVector;
Dimv miVector : { 14 ,240 , 350 };
'Obtener el tamanio de la lista
'El tamanio de la lista es 3
tamanioVector : miVector.Conteo;

DimV miCadena;
DimV miVector : { "hola" , 1 , 2 , 3 , "prueba" };
MiCadena : miVector.aTexto(); ' { hola,1,2,3,prueba }

funcion prueba(x){
	Imprimir("El valor es" + x);

	Si( a < 10 ){
		b : 23;
	}Sino{
		b : 46;
	}

	Selecciona ( a * 15 ){
		Caso 10 :
			b: 10;
		Caso 15 :
			b:15;
		Defecto :
			b:0;
	}

	Para( a : 10; a > 0; --){
		b : 23;
	}

	Mientras( a < 10 ){
		a : a + 1;
	}

	Mientras( a < 10 ){
		a : a + 1;
		Detener;
	}

	Imprimir("Hola mundo!");
}


funcion prueba2(){
	Imprimir("Hola mundo");

	'Captura de valor
	Dimv v1 , v2 , mayor;
	v1: 10;
	v2: 11;
	mayor: numeroMayor(v1,v2);

	Mensaje("Hola mundo");
	Dimv X : 10;
	Mensaje(X);

	Dimv mi_boton : Documento.Obtener("mi_boton");
	Mi_boton.setElemento("ruta", "cosas de la vida");

	Dimv mi_boton : Documento.Obtener("mi_boton");
	Mi_boton.setElemento("ruta", "mas cosas");
	Documento.Obtener("mi_imagen"). setElemento ("ruta","~/rutanueva");
}

funcion valor_mayor( x , y){
	si(x > y){
		Imprimir(x);
	}sino{
		Imprimir(y);
	}
}



funcion prueba( x , y){
	si(x > y){
		Imprimir("Variable1 es mayor que Variable2");
	}sino{
		Imprimir("Variable2 es mayor que Variable1");
	}
}

funcion numeroMayor(x,y){
	'Llamada a funciones
	Dimv v1 , v2;
	v1: 10;
	v2: 11;
	prueba(v1); 'El valor es 10
	prueba(v2); 'El valor es 11
	prueba(v1,v2); 'Variable2 es mayor que Variable1

	si(x > y){
		Retornar x;
	}sino{
		Retornar y;
	}

	Documento.Observador("listo", funcion(){
		Imprimir("Documento listo");
	});
	Dimv mi_boton : Documento.Obtener("mi_boton");
	Mi_boton.Observador("clic", mi_funcion());
}

funcion mi_funcion(){
	imprimir("Estas dentro de mi_funcion");
}