package Analizadores.CHTML;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import olc2_proyecto1.*;

public class CHTML_Arbol {

    public JPanel web_panel = new JPanel();
    public ArrayList<Componente> listaComponentes;
    public ArrayList<TError> listaErrores;

    private JPanel current_cuerpo;
    private JComboBox combo_actual;
    private DefaultTableModel modelo_actual;
    private ArrayList fila_actual;

    public CHTML_Arbol() {
        listaComponentes = new ArrayList<>();
        listaErrores = new ArrayList<>();

        this.web_panel.setBackground(Color.WHITE);
        this.web_panel.setName("Tab Pagina");
        web_panel.setLayout(new BoxLayout(web_panel, BoxLayout.PAGE_AXIS));
        current_cuerpo = web_panel;
//        this.web_panel.add(Box.createVerticalGlue());
    }

//---------------------------------------------------------------------
//-------------------------    Metodos de la clase
//---------------------------------------------------------------------    
    
    private void agregarError(int tipo, String desc) {
        System.out.println(tipo + ": " + desc);
        listaErrores.add(new TError(tipo, desc, 0, 0));
    }

    private int getIndex() {
        return listaComponentes.size();
    }

    private void revisar(Componente nd) {
        if (nd == null) {
            return;
        }

        String valor = nd.getTAG();
        System.out.println("\n" + getIndex() + ". Componente -> [" + valor + "]:");

        valor = nd.getID();
        if (valor != null) {
            System.out.println("\t ID: [" + valor + "]");
        }

        valor = nd.getGRUPO();
        if (valor != null) {
            System.out.println("\t GRUPO: [" + valor + "]");
        }

        valor = nd.getAlto() + "";
        if (valor != null) {
            System.out.println("\t Alto: [" + valor + "]");
        }

        valor = nd.getAncho() + "";
        if (valor != null) {
            System.out.println("\t Ancho: [" + valor + "]");
        }

        valor = nd.getAlineado() + "";
        if (valor != null) {
            System.out.println("\t Alineado: [" + valor + "]");
        }

        valor = nd.getClick();
        if (valor != null) {
            System.out.println("\t click: [" + valor + "]");
        }

        valor = nd.getRuta();
        if (valor != null) {
            System.out.println("\t ruta: [" + valor + "]");
        }

        valor = nd.getValor();
        if (valor != null) {
            System.out.println("\t valor: [" + valor + "]");
        }

        valor = nd.getTexto();
        if (valor != null) {
            System.out.println("\t Texto: [" + valor + "]");
        }
    }
    
    private void Redirigir(String ruta){
        if(Componente.pathExists(ruta)){
            this.web_panel.removeAll();
            this.web_panel.repaint();
            JLabel label = new JLabel(ruta);
            label.setAlignmentX(Component.LEFT_ALIGNMENT);
            web_panel.add(label);
        } else {
            System.out.println("Ruta en ENLACE[" + ruta + "] no existe");
        }
    }
    
//---------------------------------------------------------------------
//-------------------------    ACCIONES DEL ARBOL
//---------------------------------------------------------------------   

    public void AccionesCHTML() {
        Nodo raiz = CHTML_Sintactico.raiz;
        if (raiz == null) {
            System.out.println("Raiz is null");
            return;
        }

        this.web_panel.removeAll();
        this.listaComponentes.clear();

        ACCION(raiz);

        web_panel.add(Box.createVerticalGlue());
    }
    
    private void RecorrerHijos(Nodo padre) {
        for (Nodo subEtiqueta : padre.listaHijos) {
            ACCION(subEtiqueta);
        }
    }
 
    private void agregarAtributos(Componente superior, Nodo actual) {
        if (actual == null) {
            return;
        }

        switch (actual.TAG) {
            case "AT_ELEMENTOS_CCSS":
            case "BOTON_AT":
            case "IMAGEN_AT":
            case "AT_CAJA":
            case "AT_OPCION":
            case "ENLACE_AT":
                for (Nodo nAtributo : actual.listaHijos) {
                    agregarAtributos(superior, nAtributo);
                }
                break;
            case "ELEMENTO_CCSS":
                switch (actual.getSecondValue()) {
                    case "pr_id":
                        superior.setID(actual.getVALUE());
                        break;
                    case "pr_grupo":
                        superior.setGRUPO(actual.getVALUE());
                        break;
                    case "pr_alto":
                        superior.setAlto(actual.getVALUE());
                        break;
                    case "pr_ancho":
                        superior.setAncho(actual.getVALUE());
                        break;
                    case "pr_alineado":
                        superior.setAlineado(actual.getVALUE());
                        break;
                    default:
                        agregarError(2, "ELEMENTO_CCSS");
                }
                break;
            case "AT_CLICK":
                superior.setClick(actual.getVALUE());
                break;
            case "AT_RUTA":
                superior.setRuta(actual.getVALUE());
                break;
            case "AT_VALOR":
                superior.setValor(actual.getVALUE());
                break;
            default:
                agregarError(2, "ATRIBUTE: " + actual.TAG);
        }
    }

    private void ACCION(Nodo actual) {
        if (actual == null) { return; }

        switch (actual.TAG) {
            case "CHTML":
            case "ENCABEZADO":
            case "ENCABEZADO_LT":
            case "CUERPO":
            case "CUERPO_CONT":
            case "CAJA_LT":
            case "TABLA_LT":
            case "FILT_LT":
                RecorrerHijos(actual);
                break;
            case "CJS":
                System.out.println("CJS: ruta = " + actual.getVALUE());
                break;
            case "CCSS":
                System.out.println("CCSS: ruta = " + actual.getVALUE());
                break;
            case "TITULO":
                System.out.println("TITULO - txt[" + actual.getVALUE() + "]");
                web_panel.setName(actual.getVALUE());
                break;
            case "AT_FONDO":
                Color cr = Componente.stringToColor(actual.getVALUE());
                web_panel.setBackground(cr);
                break;

            //------------- INICIO SUB-TAGS CUERPO -------------//
            case "PANEL":
                Componente npanel = new Componente(current_cuerpo, "PANEL");

                if (actual.hasAtributes) {
                    agregarAtributos(npanel, actual.listaHijos.get(0));
                }
                listaComponentes.add(npanel);
                current_cuerpo = crearPanel(npanel);

                //Cont
                switch (actual.listaHijos.size()) {
                    case 2:
                        ACCION(actual.listaHijos.get(1));
                        break;
                    case 1:
                        if (!actual.hasAtributes) {
                            ACCION(actual.listaHijos.get(0));
                        }
                }

                current_cuerpo = npanel.getPadre();

                break;

            case "TEXTO":
                Componente ntexto = new Componente(current_cuerpo, "TEXTO");

                if (actual.hasVALUE()) {
                    ntexto.setTexto(actual.getVALUE());
                }

                if (actual.hasAtributes) {
                    agregarAtributos(ntexto, actual.listaHijos.get(0));
                }
                listaComponentes.add(ntexto);
                crearTexto(ntexto);
                break;

            case "BOTON":
                Componente nboton = new Componente(current_cuerpo, "BOTON");

                if (actual.hasVALUE()) {
                    nboton.setTexto(actual.getVALUE());
                }

                if (actual.hasAtributes) {
                    agregarAtributos(nboton, actual.listaHijos.get(0));
                }
                listaComponentes.add(nboton);
                crearBoton(nboton);
                break;

            case "IMAGEN":
                Componente nimagen = new Componente(current_cuerpo, "IMAGEN");

                if (actual.hasVALUE()) {
                    nimagen.setTexto(actual.getVALUE());
                }

                if (actual.hasAtributes) {
                    agregarAtributos(nimagen, actual.listaHijos.get(0));
                }
                listaComponentes.add(nimagen);
                crearImagen(nimagen);
                break;

            case "SPINNER":
                Componente nspin = new Componente(current_cuerpo, "SPINNER");

                if (actual.hasVALUE()) {
                    nspin.setTexto(actual.getVALUE());
                }

                if (actual.hasAtributes) {
                    agregarAtributos(nspin, actual.listaHijos.get(0));
                }
                listaComponentes.add(nspin);
                crearSpinner(nspin);
                break;

            case "TEXTO_A":
                Componente nArea = new Componente(current_cuerpo, "TEXTO_A");

                if (actual.hasVALUE()) {
                    nArea.setTexto(actual.getVALUE());
                }

                if (actual.hasAtributes) {
                    agregarAtributos(nArea, actual.listaHijos.get(0));
                }
                listaComponentes.add(nArea);
                crearTexto_A(nArea);
                break;

            case "CAJA_TEXTO":
                Componente nCampo = new Componente(current_cuerpo, "CAJA_TEXTO");

                if (actual.hasVALUE()) {
                    nCampo.setTexto(actual.getVALUE());
                }

                if (actual.hasAtributes) {
                    agregarAtributos(nCampo, actual.listaHijos.get(0));
                }
                listaComponentes.add(nCampo);
                crearCaja_Texto(nCampo);
                break;

            case "CAJA":
                Componente nCaja = new Componente(current_cuerpo, "CAJA");

                if (actual.hasAtributes) {
                    agregarAtributos(nCaja, actual.listaHijos.get(0));
                }
                listaComponentes.add(nCaja);
                combo_actual = new JComboBox();

                //Cont
                switch (actual.listaHijos.size()) {
                    case 2:
                        ACCION(actual.listaHijos.get(1));
                        break;
                    case 1:
                        if (!actual.hasAtributes) {
                            ACCION(actual.listaHijos.get(0));
                        }
                }

                crearCaja(nCaja);
                break;

            case "OPCION":
                Componente nOpcion = new Componente(current_cuerpo, "OPCION");

                if (actual.hasVALUE()) {
                    nOpcion.setTexto(actual.getVALUE());
                }

                if (actual.hasAtributes) {
                    agregarAtributos(nOpcion, actual.listaHijos.get(0));
                }
                revisar(nOpcion);
                listaComponentes.add(nOpcion);

                if (combo_actual != null) {
                    combo_actual.addItem(nOpcion.getValor());
                }
                break;

            case "TABLA":
                Componente nTabla = new Componente(current_cuerpo, "TABLA");

                if (actual.hasAtributes) {
                    agregarAtributos(nTabla, actual.listaHijos.get(0));
                }
                listaComponentes.add(nTabla);

                modelo_actual = new DefaultTableModel();
                fila_actual = null;

                //Cont
                switch (actual.listaHijos.size()) {
                    case 2:
                        ACCION(actual.listaHijos.get(1));
                        break;
                    case 1:
                        if (!actual.hasAtributes) {
                            ACCION(actual.listaHijos.get(0));
                        }
                }

                crearTabla(nTabla);
                break;

            case "FILT":
                Componente nFila = new Componente(current_cuerpo, "FILT");

                if (actual.hasAtributes) {
                    agregarAtributos(nFila, actual.listaHijos.get(0));
                }
                listaComponentes.add(nFila);
                fila_actual = new ArrayList<>();

                //Cont
                switch (actual.listaHijos.size()) {
                    case 2:
                        ACCION(actual.listaHijos.get(1));
                        break;
                    case 1:
                        if (!actual.hasAtributes) {
                            ACCION(actual.listaHijos.get(0));
                        }
                }
                if (!fila_actual.isEmpty()) {
                    modelo_actual.addRow(fila_actual.toArray());
                }
                break;

            case "TAB_CB":
                Componente nHeader = new Componente(current_cuerpo, "TAB_CB");

                if (actual.hasVALUE()) {
                    nHeader.setTexto(actual.getVALUE());
                }

                if (actual.hasAtributes) {
                    agregarAtributos(nHeader, actual.listaHijos.get(0));
                }
                listaComponentes.add(nHeader);

                if (modelo_actual.getRowCount() == 0) {
                    modelo_actual.addColumn(nHeader.getTexto());
                } else {
                    fila_actual.add(nHeader.getTexto());
                }
                break;

            case "TAB_CT":
                Componente nCelda = new Componente(current_cuerpo, "TAB_CT");

                if (actual.hasVALUE()) {
                    nCelda.setTexto(actual.getVALUE());
                }

                if (actual.hasAtributes) {
                    agregarAtributos(nCelda, actual.listaHijos.get(0));
                }
                listaComponentes.add(nCelda);

                fila_actual.add(nCelda.getTexto());
                break;

            case "SALTO":
                Componente nSalto = new Componente(current_cuerpo, "SALTO");
                listaComponentes.add(nSalto);
                current_cuerpo.add(Box.createRigidArea(new Dimension(10, 10)));
                break;

            case "ENLACE":
                Componente nEnlace = new Componente(current_cuerpo, "ENLACE");

                if (actual.hasVALUE()) {
                    nEnlace.setTexto(actual.getVALUE());
                }

                if (actual.hasAtributes) {
                    agregarAtributos(nEnlace, actual.listaHijos.get(0));
                }
                listaComponentes.add(nEnlace);
                crearEnlace(nEnlace);
                break;

            default:
                agregarError(2,"Tag [" + actual.TAG + "] entro a default");
                break;
        }
    }

//---------------------------------------------------------------------
//-------------------------    Crear Componentes
//---------------------------------------------------------------------

    private JPanel crearPanel(Componente comp) {
        revisar(comp);
        JPanel panel = new JPanel();

        String name = comp.getID();
        if (name != null) {
            panel.setName(name);
        } else {
            panel.setName("PANEL_" + getIndex());
        }

        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));

        panel.setPreferredSize(new Dimension(comp.getAncho(), comp.getAlto()));
        panel.setAlignmentX(comp.getAlineado());

        comp.addToPadre(panel);
        return panel;
    }

    private void crearTexto(Componente comp) {
        revisar(comp);
        JLabel label = new JLabel();

        String name = comp.getID();
        if (name != null) {
            label.setName(name);
        } else {
            label.setName("TEXTO_" + getIndex());
        }

        label.setText(comp.getTexto());
        label.setPreferredSize(new Dimension(comp.getAncho(), comp.getAlto()));
        label.setAlignmentX(comp.getAlineado());

        comp.addToPadre(label);
    }

    private void crearBoton(Componente comp) {
        revisar(comp);
        JButton boton = new JButton();

        String name = comp.getID();
        if (name != null) {
            boton.setName(name);
        } else {
            boton.setName("BOTON_" + getIndex());
        }

        boton.setText(comp.getTexto());
        boton.setPreferredSize(new Dimension(comp.getAncho(), comp.getAlto()));
        boton.setAlignmentX(comp.getAlineado());

        comp.addToPadre(boton);
    }

    private void crearImagen(Componente comp) {
        revisar(comp);

        JLabel imagen = new JLabel(comp.getImagen());

        String name = comp.getID();
        if (name != null) {
            imagen.setName(name);
        } else {
            imagen.setName("IMAGEN_" + getIndex());
        }

        imagen.setPreferredSize(new Dimension(comp.getAncho(), comp.getAlto()));
        imagen.setAlignmentX(comp.getAlineado());

        comp.addToPadre(imagen);
    }

    private void crearSpinner(Componente comp) {
        revisar(comp);

        int val = comp.getNumero();
        SpinnerModel spinnerModel = new SpinnerNumberModel(val, 0, 1000000, 1);
        JSpinner spinner = new JSpinner(spinnerModel);

        String name = comp.getID();
        if (name != null) {
            spinner.setName(name);
        } else {
            spinner.setName("SPINNER_" + getIndex());
        }

        spinner.setPreferredSize(new Dimension(comp.getAncho(), comp.getAlto()));
        spinner.setAlignmentX(comp.getAlineado());

        comp.addToPadre(spinner);
    }

    private void crearTexto_A(Componente comp) {
        revisar(comp);
        JTextArea area = new JTextArea();

        String name = comp.getID();
        if (name != null) {
            area.setName(name);
        } else {
            area.setName("TEXTO_A_" + getIndex());
        }

        area.setText(comp.getTexto());
        area.setPreferredSize(new Dimension(comp.getAncho(), comp.getAlto()));
        area.setAlignmentX(comp.getAlineado());

        comp.addToPadre(area);
    }

    private void crearCaja_Texto(Componente comp) {
        revisar(comp);
        JTextField campo = new JTextField();

        String name = comp.getID();
        if (name != null) {
            campo.setName(name);
        } else {
            campo.setName("CAJA_TEXTO_" + getIndex());
        }

        campo.setText(comp.getTexto());
        campo.setPreferredSize(new Dimension(comp.getAncho(), comp.getAlto()));
        campo.setAlignmentX(comp.getAlineado());

        comp.addToPadre(campo);
    }

    private void crearCaja(Componente comp) {
        if (combo_actual == null) {
            return;
        }
        revisar(comp);
        //cambios a combo_actual

        String name = comp.getID();
        if (name != null) {
            combo_actual.setName(name);
        } else {
            combo_actual.setName("CAJA_" + getIndex());
        }

        combo_actual.setPreferredSize(new Dimension(comp.getAncho(), comp.getAlto()));
        combo_actual.setAlignmentX(comp.getAlineado());

        comp.addToPadre(combo_actual);
    }

    private void crearTabla(Componente comp) {
        if (modelo_actual == null) {
            return;
        }
        revisar(comp);
        JTable tabla = new JTable(modelo_actual);

        String name = comp.getID();
        if (name != null) {
            tabla.setName(name);
        } else {
            tabla.setName("TABLA_" + getIndex());
        }

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setPreferredSize(new Dimension(comp.getAncho(), comp.getAlto()));
        scrollPane.setAlignmentX(comp.getAlineado());

        //tabla.setPreferredSize(new Dimension(comp.getAncho(), comp.getAlto()));
        //tabla.setAlignmentX(comp.getAlineado());
        scrollPane.setViewportView(tabla);
        comp.addToPadre(scrollPane);
    }

    private void crearEnlace(Componente comp) {
        revisar(comp);
        JButton boton = new JButton();

        boton.setForeground(Color.BLUE);
        boton.setFocusPainted(false);
        boton.setMargin(new Insets(0, 0, 0, 0));
        boton.setContentAreaFilled(false);
        boton.setBorderPainted(false);
        boton.setOpaque(false);

        String name = comp.getID();
        if (name != null) {
            boton.setName(name);
        } else {
            boton.setName("ENLACE_" + getIndex());
        }

        boton.setText(comp.getTexto());
        boton.setPreferredSize(new Dimension(comp.getAncho(), comp.getAlto()));
        boton.setAlignmentX(comp.getAlineado());

        boton.addActionListener((ActionEvent e) -> {
            Redirigir(comp.getRuta());
        });

        comp.addToPadre(boton);
    }

//---------------------------------------------------------------------
//-------------------------    Pruebas
//---------------------------------------------------------------------    
    
    private void testEventos() {
        JLabel statusLabel = new JLabel("Value: ");
        statusLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        statusLabel.setPreferredSize(new Dimension(30, 20));
        web_panel.add(statusLabel);

        SpinnerModel spinnerModel = new SpinnerNumberModel(10, //initial value
                0, //min
                100, //max
                1);//step
        JSpinner spinner = new JSpinner(spinnerModel);
        spinner.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                statusLabel.setText("Value : " + ((JSpinner) e.getSource()).getValue());
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });

        spinner.setAlignmentX(Component.CENTER_ALIGNMENT);
        spinner.setPreferredSize(new Dimension(20, 20));
        spinner.setSize(new Dimension(20, 20));

        web_panel.add(spinner);
        this.web_panel.add(Box.createVerticalGlue());
        this.web_panel.add(Box.createVerticalGlue());
    }

}
